package com.example.kjankiewicz.android_12w01_mytouchandgestures

import android.content.Context
import android.support.v7.widget.AppCompatImageView
import android.util.AttributeSet

class TouchableImageView : AppCompatImageView {
    var actionDown: Boolean = false

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        actionDown = false
    }

    constructor(context: Context) : super(context) {
        actionDown = false
    }

    override fun performClick(): Boolean {
        super.performClick()
        return true
    }
}
