package com.example.kjankiewicz.android_12w01_mytouchandgestures

import android.gesture.Gesture
import android.gesture.GestureLibraries
import android.gesture.GestureLibrary
import android.gesture.GestureOverlayView
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View

import kotlinx.android.synthetic.main.activity_my_main.*


class MyMainActivity : AppCompatActivity(), GestureOverlayView.OnGesturePerformedListener {

    private lateinit var mGestureDetector: GestureDetector
    private var gestureLib: GestureLibrary? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_main)

        imageView.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View, event: MotionEvent): Boolean {
                if (event.action == MotionEvent.ACTION_UP) {
                    val newAlpha = v.alpha + 0.5f
                    v.alpha = newAlpha
                    v.scaleX = newAlpha
                    if ((v as TouchableImageView).actionDown) {
                        v.actionDown = false
                        v.performClick()
                    }
                    return true
                }
                if (event.action == MotionEvent.ACTION_DOWN) {
                    val newAlpha = v.alpha - 0.5f
                    v.alpha = newAlpha
                    v.scaleX = newAlpha
                    (v as TouchableImageView).actionDown = true
                    return true
                }
                return false
            }
        }
        )

        mGestureDetector = GestureDetector(this,
                object : GestureDetector.SimpleOnGestureListener() {
                    override fun onFling(downME: MotionEvent,
                                         moveME: MotionEvent,
                                         velocityX: Float,
                                         velocityY: Float): Boolean {
                        if (velocityX < -5) {
                            val newRotation = imageView.rotation - 30f
                            imageView.rotation = newRotation
                        }
                        if (velocityX > 5) {
                            val newRotation = imageView.rotation + 30f
                            imageView.rotation = newRotation
                        }
                        return true
                    }
                }
        )

        val gestureOverlayView = findViewById<GestureOverlayView>(R.id.gestureOverlayView)
        gestureOverlayView.addOnGesturePerformedListener(this)
        gestureLib = GestureLibraries.fromRawResource(
                this, R.raw.gestures)
        if (!gestureLib!!.load()) {
            finish()
        }
    }

    override fun onGesturePerformed(
            overlay: GestureOverlayView,
            gesture: Gesture) {
        val predictions = gestureLib!!.recognize(gesture)
        var recognizedLetter = ""
        var maxScore = 1.0
        for (prediction in predictions) {
            if (prediction.score > maxScore) {
                when (prediction.name) {
                    "letterA" -> recognizedLetter = "A"
                    "letterB" -> recognizedLetter = "B"
                    "letterC" -> recognizedLetter = "C"
                    "letterD" -> recognizedLetter = "D"
                }
                maxScore = prediction.score
            }
        }
        textView.text = String.format("%s%s",
                textView.text, recognizedLetter)
    }

    override fun onTouchEvent(motionEvent: MotionEvent): Boolean {
        return mGestureDetector.onTouchEvent(motionEvent)
    }

    /*    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {
        Toast.makeText(this,"X:" + motionEvent.getX() +
                " Y:" + motionEvent.getY(), Toast.LENGTH_LONG).show();
        return true;
    }  */
}
